import { useState } from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import { List, ListItemButton, ListItemIcon, ListItemText } from '@mui/material';

// assets
import {
  CommentOutlined,
  LockOutlined,
  QuestionCircleOutlined,
  UserOutlined,
  UnorderedListOutlined,
  RobotOutlined
} from '@ant-design/icons';
import { Switch } from '../../../../../../node_modules/@mui/material/index';
import api from 'utils/api';
import { useDispatch } from 'react-redux';
import { setUser } from 'store/reducers/auth';

// ==============================|| HEADER PROFILE - SETTING TAB ||============================== //

const SettingTab = ({ botIsRunning }) => {
  const dispatch = useDispatch();
  const theme = useTheme();

  const [selectedIndex, setSelectedIndex] = useState(0);
  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
  };

  const getUser = async () => {
    try {
      const res = await api.get('user/info');

      dispatch(setUser(res.data));
    } catch (err) {
      console.log(err);
    }
  };
  const handleToggleBot = () => {
    if (!botIsRunning) {
      api
        .post('user/startBot')
        .then((res) => {
          getUser();
          alert(res.data.key.otp);
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      api
        .post('user/stopBot')
        .then((res) => {
          getUser();
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  return (
    <List component="nav" sx={{ p: 0, '& .MuiListItemIcon-root': { minWidth: 32, color: theme.palette.grey[500] } }}>
      <ListItemButton>
        <ListItemIcon>
          <RobotOutlined />
        </ListItemIcon>
        <div
          style={{
            display: 'flex',
            alignItems: 'center'
          }}
        >
          <ListItemText primary="Run Bot" />
          <Switch checked={botIsRunning} onClick={() => handleToggleBot()} />
        </div>
      </ListItemButton>
      <ListItemButton selected={selectedIndex === 0} onClick={(event) => handleListItemClick(event, 0)}>
        <ListItemIcon>
          <QuestionCircleOutlined />
        </ListItemIcon>
        <ListItemText primary="Support" />
      </ListItemButton>
      <ListItemButton selected={selectedIndex === 1} onClick={(event) => handleListItemClick(event, 1)}>
        <ListItemIcon>
          <UserOutlined />
        </ListItemIcon>
        <ListItemText primary="Account Settings" />
      </ListItemButton>
      <ListItemButton selected={selectedIndex === 2} onClick={(event) => handleListItemClick(event, 2)}>
        <ListItemIcon>
          <LockOutlined />
        </ListItemIcon>
        <ListItemText primary="Privacy Center" />
      </ListItemButton>
      <ListItemButton selected={selectedIndex === 3} onClick={(event) => handleListItemClick(event, 3)}>
        <ListItemIcon>
          <CommentOutlined />
        </ListItemIcon>
        <ListItemText primary="Feedback" />
      </ListItemButton>
      <ListItemButton selected={selectedIndex === 4} onClick={(event) => handleListItemClick(event, 4)}>
        <ListItemIcon>
          <UnorderedListOutlined />
        </ListItemIcon>
        <ListItemText primary="History" />
      </ListItemButton>
    </List>
  );
};

export default SettingTab;
