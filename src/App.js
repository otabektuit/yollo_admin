// project import
import Routes from 'routes';
import ThemeCustomization from 'themes';
import ScrollTop from 'components/ScrollTop';
import { QueryClientProvider, QueryClient } from 'react-query';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor } from 'store/index';
import './index.css';

// ==============================|| APP - THEME, ROUTER, LOCAL  ||============================== //

const queryClient = new QueryClient({});

const App = () => (
  <ThemeCustomization>
    <PersistGate loading={null} persistor={persistor}>
      <QueryClientProvider client={queryClient}>
        <ScrollTop>
          <Routes />
        </ScrollTop>
      </QueryClientProvider>
    </PersistGate>
  </ThemeCustomization>
);

export default App;
