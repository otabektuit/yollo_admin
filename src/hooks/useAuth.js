import { useMutation } from 'react-query';
import api from 'utils/api';

const useAuth = () => {
  const login = useMutation((data) => api.post('auth/login', data));
  const register = useMutation((data) => api.post('auth/register', data));

  return { login, register };
};

export default useAuth;
