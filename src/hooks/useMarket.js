import { useQuery, useMutation } from 'react-query';
import api from 'utils/api';

const useMarket = (props) => {
  const { data, isLoading, refetch } = useQuery('GET_MARKETS', () => api.get('market/getLists'), {
    enabled: !!props?.page && !!props?.amount
  });

  const createMarket = useMutation((data) => api.post('market/create', data));

  return {
    data: data?.data || [],
    isLoading,
    refetch,
    createMarket
  };
};

export default useMarket;
