import { useQuery, useMutation } from 'react-query';
import api from 'utils/api';

const useProduct = (props) => {
  const { data, isLoading, refetch } = useQuery(
    ['GET_PRODUCTS'],
    () =>
      api.get('product/getList', {
        perPage: props?.amount,
        page: props?.page
      }),
    {
      enabled: !!props?.amount && !!props?.page
    }
  );

  const createProduct = useMutation((data) => api.post('product/create', data));
  const deleteProduct = useMutation((id) => api.delete('product?productId=' + id));
  const updateProduct = useMutation(({ payload, id }) => api.patch('product?productId=' + id, payload));

  return { data, isLoading, refetch, createProduct, updateProduct, deleteProduct };
};

export default useProduct;
