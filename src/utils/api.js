import axios from 'axios';
import { store } from 'store/index';

const API_URL = 'https://api.yollo.io/';
const API_SUFFIX = 'api/v1/';

const api = axios.create({
  baseURL: [API_URL, API_SUFFIX].join('')
});

const errorHandler = (error, hooks) => {
  const status = error.response.status;
  if (status === 401) {
    // window.location.reload();
  }

  return Promise.reject(error.response);
};

api.interceptors?.request.use(
  (config) => {
    const token = store.getState()?.auth?.token?.access?.token;

    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }

    return config;
  },

  (error) => errorHandler(error)
);

api.interceptors?.response.use((response) => response.data, errorHandler);

export default api;
