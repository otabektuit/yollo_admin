import { Box, Grid, Typography } from '../../../../node_modules/@mui/material/index';
import { Button, Form, Input, InputNumber, Upload } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import useMarket from 'hooks/useMarket';
import { useState } from 'react';
import { useNavigate } from '../../../../node_modules/react-router-dom/dist/index';

const layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 16
  }
};

/* eslint-disable no-template-curly-in-string */
const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!'
  },
  number: {
    range: '${label} must be between ${min} and ${max}'
  }
};
/* eslint-enable no-template-curly-in-string */

const CreateMarket = () => {
  const { createMarket } = useMarket();
  const navigate = useNavigate();
  const [image, setImage] = useState(null);

  const normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }

    setImage(e?.file?.originFileObj);
    return e?.fileList;
  };

  const onFinish = ({ product }) => {
    const formData = new FormData();

    formData.append('marketName', product.marketName);
    formData.append('marketUrl', product.marketUrl);
    formData.append('marketLogoImage', product.marketLogoImage?.[0].originFileObj);

    createMarket.mutate(formData, {
      onSuccess: () => {
        alert('Successfuly create');
        navigate('/market');
      },
      onError: () => {
        alert('Error in creation');
      }
    });
  };

  return (
    <Grid container rowSpacing={4.5} columnSpacing={2.75}>
      <Grid item xs={12} sx={{ mb: -2.25 }}>
        <Typography variant="h5">Create Market</Typography>
      </Grid>
      <Grid item xs={12}>
        <Form
          {...layout}
          name="nest-messages"
          onFinish={onFinish}
          style={{
            maxWidth: '100%',
            display: 'flex',
            gap: '15px'
          }}
          validateMessages={validateMessages}
        >
          <Box width="50%">
            <Form.Item
              name={['product', 'marketName']}
              label="Name"
              rules={[
                {
                  required: true
                }
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name={['product', 'marketUrl']}
              label="URL"
              rules={[
                {
                  required: true
                }
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                ...layout.wrapperCol,
                offset: 8
              }}
            >
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Box>
          <Box width="50%">
            <Form.Item name={['product', 'marketLogoImage']} valuePropName="fileList" getValueFromEvent={normFile} noStyle>
              <Upload.Dragger name="files" action="/upload.do">
                <p className="ant-upload-drag-icon">
                  <InboxOutlined />
                </p>
                <p className="ant-upload-text">Click or drag file to this area to upload</p>
                <p className="ant-upload-hint">Support for a single or bulk upload.</p>
              </Upload.Dragger>
            </Form.Item>
          </Box>
        </Form>
      </Grid>
    </Grid>
  );
};

export default CreateMarket;
