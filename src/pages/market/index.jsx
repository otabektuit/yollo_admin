import { Grid, Typography, Button } from '@mui/material';
import { PlusOutlined } from '@ant-design/icons';
import { Table } from 'antd';
import { useNavigate } from '../../../node_modules/react-router-dom/dist/index';
import useMarket from 'hooks/useMarket';

const dataSource = [
  {
    key: '1',
    id: `1`,
    name: 'Apple',
    url: 'https://apple.com/123'
  },
  {
    key: '2',
    id: `2`,
    name: 'Samsung',
    url: 'https://samsung.com/123'
  }
];

const columns = [
  {
    title: 'Name',
    dataIndex: 'marketName',
    key: 'marketName'
  },
  {
    title: 'URL',
    dataIndex: 'marketUrl',
    key: 'marketUrl'
  },
  {
    title: 'Logo',
    dataIndex: 'marketLogoImage',
    key: 'marketLogoImage',
    render: (data) => {
      console.log(data);

      return <img src={data} alt="logo" width="70px" />;
    }
  }
];

const Markets = () => {
  const navigate = useNavigate();
  const { data } = useMarket({ page: 1, amount: 20 });
  console.log(data);
  return (
    <Grid container rowSpacing={4.5} columnSpacing={2.75}>
      <Grid item xs={8} sx={{ mb: -2.25 }}>
        <Typography variant="h5">Markets</Typography>
      </Grid>
      <Grid item xs={4} sx={{ mb: -2.25, display: 'flex', justifyContent: 'flex-end' }}>
        <Button
          variant="contained"
          aria-label="add"
          style={{
            width: '100px'
          }}
          onClick={() => navigate('/market/create')}
        >
          <PlusOutlined style={{ marginRight: '10px' }} /> Market
        </Button>
      </Grid>
      <Grid item xs={12}>
        <Table
          dataSource={data || dataSource}
          columns={columns}
          onRow={(record, rowIndex) => {
            return {
              onClick: () => {
                navigate(record.id);
              } // click row
            };
          }}
        />
        ;
      </Grid>
    </Grid>
  );
};

export default Markets;
