import { Box, Grid, Typography } from '../../../../node_modules/@mui/material/index';
import { Button, Form, Input, InputNumber, Upload } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import { Select, Space } from 'antd';
import useProduct from 'hooks/useProduct';
import useMarket from 'hooks/useMarket';
import { useNavigate, useParams } from '../../../../node_modules/react-router-dom/dist/index';
import { Editor, EditorTools } from '@progress/kendo-react-editor';
import { useEffect, useState } from 'react';
import '@progress/kendo-theme-default/dist/all.css';
import { useQuery } from 'react-query';
import api from 'utils/api';

const {
  Bold,
  Italic,
  Underline,
  Strikethrough,
  Subscript,
  Superscript,
  AlignLeft,
  AlignCenter,
  AlignRight,
  AlignJustify,
  Indent,
  Outdent,
  OrderedList,
  UnorderedList,
  Undo,
  Redo,
  FontSize,
  FontName,
  FormatBlock,
  Link,
  Unlink,
  InsertImage,
  ViewHtml,
  InsertTable,
  AddRowBefore,
  AddRowAfter,
  AddColumnBefore,
  AddColumnAfter,
  DeleteRow,
  DeleteColumn,
  DeleteTable,
  MergeCells,
  SplitCell
} = EditorTools;
const { Option } = Select;

const layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 16
  }
};

/* eslint-disable no-template-curly-in-string */
const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!'
  },
  number: {
    range: '${label} must be between ${min} and ${max}'
  }
};
/* eslint-enable no-template-curly-in-string */

const normFile = (e) => {
  console.log('Upload event:', e);
  if (Array.isArray(e)) {
    return e;
  }
  return e?.fileList;
};

const CreateProduct = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [form] = Form.useForm();

  const { createProduct, updateProduct, deleteProduct } = useProduct();

  const { data, refetch } = useQuery(['GET_PRODUCTS'], () => api.get('product?productId=' + id), {
    enabled: !!id
  });

  const { data: markets } = useMarket({ amount: 20, page: 1 });

  const [videoUrls, setVideoUrls] = useState([]);
  const [videoVal, setVideoVal] = useState('');
  const [content, setContent] = useState('');
  const [contentKor, setContentKor] = useState('');

  useEffect(() => {
    if (!data?.data) return;

    setContent(data?.data?.description?.en || '');
    setContentKor(data?.data?.description?.kr || '');
    setVideoUrls(data?.data?.videoUrl || []);

    form.setFieldsValue({
      product: {
        productNameEn: data?.data?.productName?.en,
        productNameKor: data?.data?.productName?.kr,
        pixelID: data.data.pixelID,
        stars: data.data.stars,
        purchasedCount: data.data.productPurchuased,
        productUrl: data.data.productUrl,
        discount: data.data.discount,
        currentPrice: data.data.currentPrice,
        image: data?.data?.productImages?.map((_img) => ({ name: _img, preventSend: true }))
      },
      market: data.data.marketId
    });
  }, [data]);

  const onChange = (event) => {
    setContent(event.html);
  };

  const onChangeKor = (event) => {
    setContentKor(event.html);
  };

  const onFinish = (values) => {
    const formData = new FormData();

    formData.append('productName[en]', values.product.productNameEn);
    formData.append('productName[kr]', values.product.productNameKor);
    formData.append('description[en]', content);
    formData.append('description[kr]', contentKor);

    formData.append('pixelID', values.product.pixelID);

    formData.append('stars', values.product.stars || 5);
    formData.append('productPurchuased', values.product.purchasedCount || 11);
    formData.append('productUrl', values.product.productUrl);

    formData.append('discount', values.product.discount);
    formData.append('currentPrice', values.product.currentPrice);
    formData.append('marketId', values.market);
    videoUrls.map((videourl) => formData.append('videoUrl', videourl));

    const filteredImages = values.product.image.filter((_img) => !_img.preventSend);

    filteredImages.forEach((img) => formData.append('productImages', img.originFileObj));

    if (id)
      updateProduct.mutate(
        { payload: formData, id },
        {
          onSuccess: (res) => {
            navigate('/products');
          },
          onError: (err) => {
            alert(err?.data?.message);
          }
        }
      );
    else
      createProduct.mutate(formData, {
        onSuccess: (res) => {
          navigate('/products');
        },
        onError: (err) => {
          alert(err?.data?.message);
        }
      });
  };

  const onChangeVideoUrl = (e) => setVideoVal(e.target.value);

  const onAddVideoUrl = () => {
    if (!videoVal) return;
    setVideoUrls((prev) => [...prev, videoVal]);
    setVideoVal('');
  };

  const onDelete = () => {
    deleteProduct.mutateAsync(id, {
      onSuccess: (res) => {
        navigate('/products');
      },
      onError: (err) => {
        alert(err?.data?.message);
      }
    });
  };

  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };

  return (
    <Grid container rowSpacing={4.5} columnSpacing={2.75}>
      <Grid item xs={12} sx={{ mb: -2.25 }}>
        <Typography variant="h5">Create Product</Typography>
      </Grid>
      <Grid item xs={12}>
        <Form
          {...layout}
          form={form}
          name="nest-messages"
          onFinish={onFinish}
          style={{
            maxWidth: '100%',
            display: 'flex',
            gap: '15px'
          }}
          validateMessages={validateMessages}
        >
          <Box width="70%">
            <Form.Item
              name={['product', 'productNameEn']}
              label="Name En"
              defaultValue={data?.data?.productName?.kr}
              rules={[
                {
                  required: true
                }
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name={['product', 'productNameKor']}
              label="Name Kor"
              rules={[
                {
                  required: true
                }
              ]}
            >
              <Input value={data?.data?.productName?.kr} />
            </Form.Item>
            <Form.Item
              // name={['product', 'descriptionEn']}
              label="Description En"
              // rules={[
              //   {
              //     required: true
              //   }
              // ]}
            >
              {/* <Input.TextArea /> */}
              <Editor
                tools={[
                  [Bold, Italic, Underline, Strikethrough],
                  [Subscript, Superscript],
                  [AlignLeft, AlignCenter, AlignRight, AlignJustify],
                  [Indent, Outdent],
                  [OrderedList, UnorderedList],
                  FontSize,
                  FontName,
                  FormatBlock,
                  [Undo, Redo],
                  [Link, Unlink, InsertImage, ViewHtml],
                  [InsertTable],
                  [AddRowBefore, AddRowAfter, AddColumnBefore, AddColumnAfter],
                  [DeleteRow, DeleteColumn, DeleteTable],
                  [MergeCells, SplitCell]
                ]}
                contentStyle={{
                  height: 230
                }}
                value={content}
                onChange={onChange}
              />
            </Form.Item>
            <Form.Item
              // name={['product', 'descriptionKor']}
              label="Description Kor"
              // rules={[
              //   {
              //     required: true
              //   }
              // ]}
            >
              {/* <Input.TextArea /> */}
              <Editor
                tools={[
                  [Bold, Italic, Underline, Strikethrough],
                  [Subscript, Superscript],
                  [AlignLeft, AlignCenter, AlignRight, AlignJustify],
                  [Indent, Outdent],
                  [OrderedList, UnorderedList],
                  FontSize,
                  FontName,
                  FormatBlock,
                  [Undo, Redo],
                  [Link, Unlink, InsertImage, ViewHtml],
                  [InsertTable],
                  [AddRowBefore, AddRowAfter, AddColumnBefore, AddColumnAfter],
                  [DeleteRow, DeleteColumn, DeleteTable],
                  [MergeCells, SplitCell]
                ]}
                contentStyle={{
                  height: 230
                }}
                value={contentKor}
                onChange={onChangeKor}
              />
            </Form.Item>
            <Form.Item
              name={['product', 'pixelID']}
              label="Pixel ID"
              rules={[
                {
                  required: true
                }
              ]}
            >
              <Input.TextArea />
            </Form.Item>
            <Form.Item
              name={['product', 'discount']}
              label="Discount %"
              rules={[
                {
                  type: 'number',
                  min: 0
                }
              ]}
            >
              <InputNumber />
            </Form.Item>
            <Form.Item
              name={['product', 'currentPrice']}
              label="Current Price"
              rules={[
                {
                  type: 'number',
                  min: 0,
                  required: true
                }
              ]}
            >
              <InputNumber />
            </Form.Item>

            <Form.Item
              name={['product', 'stars']}
              label="Rate stars"
              rules={[
                {
                  type: 'number',
                  min: 0,
                  max: 5,
                  required: true
                }
              ]}
            >
              <InputNumber />
            </Form.Item>

            <Form.Item
              name={['product', 'purchasedCount']}
              label="Purchase number"
              rules={[
                {
                  type: 'number',
                  min: 0,
                  required: true
                }
              ]}
            >
              <InputNumber />
            </Form.Item>

            <Form.Item
              name="market"
              label="Market"
              rules={[
                {
                  required: true
                }
              ]}
            >
              <Select placeholder="Select a market" allowClear>
                {markets.map((market) => (
                  <Option value={market.id} key={market.id}>
                    {market.marketName}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name={['product', 'productUrl']}
              label="URL"
              rules={[
                {
                  required: true
                }
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name={['product', 'videoUrl']}
              label="Video URLs"
              rules={[
                {
                  required: false
                }
              ]}
            >
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  gap: '5px',
                  margin: '5px 0'
                }}
              >
                {videoUrls.map((video, v) => (
                  <div
                    key={v}
                    style={{
                      display: 'flex',
                      height: 30,
                      alignItems: 'center'
                    }}
                  >
                    <p>{video}</p>{' '}
                    <Button
                      onClick={() => setVideoUrls((prev) => prev.filter((_, idx) => idx !== v))}
                      style={{
                        marginLeft: '10px'
                      }}
                    >
                      x
                    </Button>
                  </div>
                ))}
              </div>
              <Input value={videoVal} onChange={onChangeVideoUrl} /> <Button onClick={onAddVideoUrl}>Add</Button>
            </Form.Item>

            <Form.Item label="Images">
              <Form.Item name={['product', 'image']} valuePropName="fileList" getValueFromEvent={normFile} noStyle>
                <Upload.Dragger name="files" action="/upload.do">
                  <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                  </p>
                  <p className="ant-upload-text">Click or drag file to this area to upload</p>
                  <p className="ant-upload-hint">Support for a single or bulk upload.</p>
                </Upload.Dragger>
              </Form.Item>
            </Form.Item>

            <Form.Item
              wrapperCol={{
                ...layout.wrapperCol,
                offset: 8
              }}
            >
              <Button
                type="primary"
                htmlType="submit"
                loading={createProduct.isLoading || updateProduct.isLoading}
                disabled={deleteProduct.isLoading}
              >
                Submit
              </Button>

              {!!id && (
                <Button
                  type="secondary"
                  htmlType="button"
                  loading={deleteProduct.isLoading}
                  onClick={onDelete}
                  disabled={createProduct.isLoading || updateProduct.isLoading}
                >
                  Delete
                </Button>
              )}
            </Form.Item>
          </Box>
        </Form>
      </Grid>
    </Grid>
  );
};

export default CreateProduct;
