import { Grid, Typography, Button } from '@mui/material';
import { PlusOutlined } from '@ant-design/icons';
import { useNavigate } from '../../../node_modules/react-router-dom/dist/index';
import { Select, Input, Table } from 'antd';
import { Box } from '../../../node_modules/@mui/material/index';
import useProduct from 'hooks/useProduct';
import { productBaseUrl } from 'utils/const';
const options = [];

options.push({
  label: 'Apple',
  value: 1
});

options.push({
  label: 'Samsung',
  value: 2
});

const handleChange = (value) => {
  console.log(`selected ${value}`);
};

const onSearch = (value, _e, info) => console.log(info?.source, value);

const columns = [
  {
    title: 'Name EN',
    dataIndex: 'productName',
    key: 'productName',
    render: (data) => {
      return <span>{data?.en}</span>;
    }
  },
  {
    title: 'Name KR',
    dataIndex: 'productName',
    key: 'productName',
    render: (data) => {
      return <span>{data?.kr}</span>;
    }
  },
  {
    title: 'Market',
    dataIndex: 'marketId',
    key: 'marketId',
    render: (data) => {
      return <span>{data.marketName}</span>;
    }
  },
  {
    title: 'Price',
    dataIndex: 'currentPrice',
    key: 'currentPrice'
  },
  {
    title: 'URL',
    dataIndex: 'productUrl',
    key: 'productUrl'
  },
  {
    title: 'Purchased',
    dataIndex: 'productPurchuased',
    key: 'productPurchuased'
  },
  {
    title: 'Website URL',
    dataIndex: 'id',
    key: 'id',
    render: (data) => {
      return (
        <a href={`${productBaseUrl}product/${data}`} target="_blank" rel="noreferrer">
          {productBaseUrl}product/{data}
        </a>
      );
    }
  }
];

const Products = () => {
  const navigate = useNavigate();

  const { data } = useProduct({ amount: 20, page: 1 });

  return (
    <Grid container rowSpacing={4.5} columnSpacing={2.75}>
      <Grid item xs={8} sx={{ mb: -2.25 }}>
        <Typography variant="h5">Products</Typography>
      </Grid>
      <Grid item xs={4} sx={{ mb: -2.25, display: 'flex', justifyContent: 'flex-end' }}>
        <Button
          variant="contained"
          aria-label="add"
          style={{
            width: '100px'
          }}
          onClick={() => navigate('/products/create')}
        >
          <PlusOutlined style={{ marginRight: '10px' }} /> Product
        </Button>
      </Grid>

      <Grid
        item
        xs={12}
        style={{
          display: 'flex'
        }}
      >
        <Box width={200} mr={2}>
          <Input.Search
            placeholder="input search text"
            onSearch={onSearch}
            style={{
              width: 200
            }}
          />
        </Box>
        <Box width={200}>
          <Select
            mode="multiple"
            allowClear
            style={{
              width: '100%'
            }}
            placeholder="Select markets"
            onChange={handleChange}
            options={options}
          />
        </Box>
      </Grid>
      <Grid item xs={12}>
        <Table
          dataSource={data?.data}
          columns={columns}
          onRow={(item) => {
            return {
              onClick: () => navigate('/products/' + item.id)
            };
          }}
        />
        ;
      </Grid>
    </Grid>
  );
};

export default Products;
