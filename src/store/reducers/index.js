// third-party
import { combineReducers } from 'redux';

// project import
import menu from './menu';
import auth from './auth';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';

// ==============================|| COMBINE REDUCERS ||============================== //

const authPersistConfig = {
  key: 'auth',
  storage
};

const authPersistedReducer = persistReducer(authPersistConfig, auth);

const reducers = combineReducers({ menu, auth: authPersistedReducer });

export default reducers;
