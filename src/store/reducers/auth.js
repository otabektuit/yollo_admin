// types
import { createSlice } from '@reduxjs/toolkit';

// initial state
const initialState = {
  user: null,
  token: null
};

// ==============================|| SLICE - MENU ||============================== //

const auth = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setUser(state, { payload }) {
      state.user = payload;
    },

    setToken(state, { payload }) {
      state.token = payload;
    }
  }
});

export default auth.reducer;

export const { setToken, setUser } = auth.actions;
