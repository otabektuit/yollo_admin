import { lazy } from 'react';

// project import
import Loadable from 'components/Loadable';
import MainLayout from 'layout/MainLayout';
import Products from 'pages/products/index';
import CreateProduct from 'pages/products/create/index';
import Markets from 'pages/market/index';
import CreateMarket from 'pages/market/create/index';
import { Navigate } from '../../node_modules/react-router-dom/dist/index';

// render - dashboard
const DashboardDefault = Loadable(lazy(() => import('pages/dashboard')));

// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
  path: '/',
  element: <MainLayout />,
  children: [
    {
      path: '/',
      element: <DashboardDefault />
    },
    {
      path: 'dashboard',
      children: [
        {
          path: 'default',
          element: <DashboardDefault />
        }
      ]
    },
    {
      path: 'products',
      children: [
        {
          index: true,
          element: <Products />
        },
        {
          path: 'create',
          element: <CreateProduct />
        },
        {
          path: ':id',
          element: <CreateProduct />
        }
      ]
    },
    {
      path: 'market',
      children: [
        {
          index: true,
          element: <Markets />
        },
        {
          path: 'create',
          element: <CreateMarket />
        },
        {
          path: ':id',
          element: <CreateMarket />
        }
      ]
    },
    {
      path: '*',
      element: <Navigate to="/" />
    }
  ]
};

export default MainRoutes;
