import { useRoutes } from 'react-router-dom';

// project import
import LoginRoutes from './LoginRoutes';
import MainRoutes from './MainRoutes';
import { useSelector } from 'react-redux';
import { useMemo } from 'react';

// ==============================|| ROUTING RENDER ||============================== //

export default function ThemeRoutes() {
  const { user, token } = useSelector((store) => store.auth);

  const routes = useMemo(() => (!!user && !!token ? MainRoutes : LoginRoutes), [user, token]);

  return useRoutes([routes]);
}
